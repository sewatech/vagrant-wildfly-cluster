# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'getoptlong'

opts = GetoptLong.new(
  ## Vagrant options
  [ '--force', '-f', GetoptLong::NO_ARGUMENT ],
  [ '--prune', GetoptLong::NO_ARGUMENT ],
  [ '--provision', GetoptLong::NO_ARGUMENT ],
  ## Custom options
  [ '--vms', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--cluster', GetoptLong::REQUIRED_ARGUMENT ]
)

vms=2
cluster='simple'

opts.each do |opt, arg|
  case opt
    when '--vms'
      vms=arg.to_i
    when '--cluster'
      cluster=arg
  end
end

Vagrant.configure("2") do |config|
  # See https://docs.vagrantup.com.
  config.vm.box = "generic/debian9"
  config.vm.synced_folder "./shared", "/home/vagrant/shared"

  case cluster
    when 'simple'
      (1..vms).each do |i|
        config.vm.define "wf#{i}" do |node|
          node.vm.hostname = "wf#{i}"
          node.vm.network :private_network, ip: "192.168.33.#{10+i}"
          node.vm.provision "shell", 
            inline: "/home/vagrant/shared/init-vm.sh #{i}"
        end
      end
    
    when 'colocated'
      (1..vms).each do |i|
        config.vm.define "wf#{i}" do |node|
          node.vm.hostname = "wf#{i}"
          node.vm.network :private_network, ip: "192.168.33.#{10+i}"
          node.vm.provision "shell", 
            inline: "/home/vagrant/shared/colocated.sh #{i}"
        end
      end

    when 'replicated'
      config.vm.define "main" do |node|
        node.vm.hostname = "main"
        node.vm.network :private_network, ip: "192.168.33.11"
        node.vm.provision "shell", 
          inline: "/home/vagrant/shared/replicated-main.sh 1"
      end

      config.vm.define "backup" do |node|
        node.vm.hostname = "backup"
        node.vm.network :private_network, ip: "192.168.33.12"
        node.vm.provision "shell", 
          inline: "/home/vagrant/shared/replicated-backup.sh 2"
      end
    
    else
      puts "Wrong --cluster value"
      abort

  end

end

