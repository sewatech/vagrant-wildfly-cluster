home_dir=$(pwd)

apt install -qq -y apache2 > /dev/null

cp $home_dir/shared/apache/proxy.conf /etc/apache2/conf-available/
a2enmod proxy proxy_http proxy_balancer lbmethod_byrequests
a2enconf proxy
systemctl restart apache2

