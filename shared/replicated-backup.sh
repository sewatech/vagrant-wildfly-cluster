home_dir=$(pwd)
wildfly_dir=/opt/wildfly

source $home_dir/shared/init-vm.sh $1

rm $wildfly_dir/standalone/deployments/msg.war
$wildfly_dir/bin/jboss-cli.sh --file=$home_dir/shared/cli-scripts/replicated-backup.cli

