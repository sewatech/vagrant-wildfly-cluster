home_dir=$(pwd)
wildfly_dir=/opt/wildfly

source $home_dir/shared/init-vm.sh $1

$wildfly_dir/bin/jboss-cli.sh --file=$home_dir/shared/cli-scripts/colocated.cli --properties=$home_dir/shared/vm-config/system$1.properties

