home_dir=$(pwd)
wildfly_version=24.0.1.Final
#wildfly_version=15.0.1.Final
wildfly_dir=/opt/wildfly
ip_address=$(hostname -I | cut -d " " -f 2)
vm_id=$1

apt update
apt install -qq -y openjdk-8-jdk tcpdump > /dev/null
ip route add 224.0.0.0/4 dev eth1
sudo groupadd -r wildfly
useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly

wget --quiet https://download.jboss.org/wildfly/$wildfly_version/wildfly-$wildfly_version.tar.gz 
tar -xf wildfly-$wildfly_version.tar.gz --directory /opt
#tar -xf shared/wildfly-$wildfly_version.tar.gz --directory /opt
mv /opt/wildfly-$wildfly_version $wildfly_dir
sed -e "s/0.0.0.0/$ip_partner/" shared/wf-files/standalone.conf > /opt/wildfly/bin/standalone.conf
cp shared/wf-files/msg.war $wildfly_dir/standalone/deployments/
cp shared/wf-files/sw-*.properties $wildfly_dir/standalone/configuration/

mkdir -p /etc/wildfly
#cp $wildfly_dir/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/
sed -e "s/0.0.0.0/$ip_address/" $wildfly_dir/docs/contrib/scripts/systemd/wildfly.conf > /etc/wildfly/wildfly.conf
cp $wildfly_dir/docs/contrib/scripts/systemd/launch.sh $wildfly_dir/bin/
chmod +x $wildfly_dir/bin/*.sh
cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/

rm $wildfly_dir/bin/*.bat
rm $wildfly_dir/bin/*.ps1
rm $wildfly_dir/bin/domain.*

mv $wildfly_dir/standalone/configuration/standalone-full-ha.xml $wildfly_dir/standalone/configuration/standalone.xml
rm $wildfly_dir/standalone/configuration/standalone-*.xml

$wildfly_dir/bin/add-user.sh admin admpwd
rm -rf $wildfly_dir/domain/
$wildfly_dir/bin/jboss-cli.sh --file=$home_dir/shared/cli-scripts/init.cli --properties=$home_dir/shared/vm-config/system$vm_id.properties
chown -RH wildfly: $wildfly_dir

systemctl daemon-reload
systemctl enable wildfly
systemctl start wildfly

source $home_dir/shared/apache.sh

